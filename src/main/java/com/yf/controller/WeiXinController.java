package com.yf.controller;

import com.yf.util.MessageUtil;
import com.yf.util.WxUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

/**
 * <Description> <br>
 *
 * @author yinfeng<br>
 * @version 1.0<br>
 * @CreateDate 2018/7/31 <br>
 */
@RestController
@RequestMapping("/weChart")
public class WeiXinController {


    /**
     * logger <br>
     */
    private static Logger logger = LoggerFactory.getLogger(WeiXinController.class);

    /**
     * Description: 心跳接口<br>
     *
     * @param request HttpServletRequest
     * @return <br>
     * @author yinfeng<br>
     * @taskId <br>
     */
    @RequestMapping(value = "/test", produces = "application/json;charset=utf-8")
    public String test(HttpServletRequest request) {
        logger.info("123");
        return "123";
    }


    /**
     * Description: 微信服务器认证校验<br>
     *
     * @param request HttpServletRequest
     * @return <br>
     * @author yinfeng<br>
     * @taskId <br>
     */
    @GetMapping(value = "/weixinCheck", produces = "application/json;charset=utf-8")
    public String weixinCheck(HttpServletRequest request) {
        logger.info("================weixinCheck get 被调用");
        String token = "yinfeng";
        //接收微信服务器三个参数
        String signature = request.getParameter("signature");
        String timestamp = request.getParameter("timestamp");
        String nonce = request.getParameter("nonce");
        String echostr = request.getParameter("echostr");
        logger.info("================weixinCheck param signature{},timestamp{},nonce{},echostr{}", new String[]{signature, timestamp, nonce, echostr});
        //进行验证
        try {
            boolean res = WxUtils.checkSingature(signature, token, timestamp, nonce);
            logger.info("================weixinCheck checkSingature rest{}", res);
            if (res) {
                //返回echostr
                return echostr;
            }
        } catch (Exception e) {
            logger.error("weixinCheck error", e);
        }
        return "";
    }


    /**
     * Description: 微信业务调用接口<br>
     *
     * @param request HttpServletRequest
     * @return <br>
     * @author yinfeng<br>
     * @taskId <br>
     */
    @PostMapping(value = "/weixinCheck", produces = "application/json;charset=utf-8")
    public String receiveWxMessage(@RequestBody String message, HttpServletRequest request) {
        logger.info("================weixinCheck post 被调用");
        String token = "yinfeng";
        //接收微信服务器三个参数
        String signature = request.getParameter("signature");
        String timestamp = request.getParameter("timestamp");
        String nonce = request.getParameter("nonce");
        String echostr = request.getParameter("echostr");
        logger.info("================weixinCheck param signature{},timestamp{},nonce{},echostr{},message{}", new String[]{signature, timestamp, nonce, echostr, message});

        try {
            //进行验证
            boolean res = WxUtils.checkSingature(signature, token, timestamp, nonce);
            logger.info("================weixinCheck checkSingature rest{}", res);
            if (res) {
                Map<String, String> stringStringMap = MessageUtil.parseXml(message);
                // 发送方帐号（open_id）
                String fromUserName = stringStringMap.get("FromUserName");
                // 公众帐号
                String toUserName = stringStringMap.get("ToUserName");
                // 消息类型
                String msgType = stringStringMap.get("MsgType");
                // 消息内容
                String content = stringStringMap.get("Content");
                //普通文本消息
                if ("text".equals(msgType)) {
                    String recMessage = "<xml><ToUserName><![CDATA["+fromUserName+"]]></ToUserName><FromUserName><![CDATA["+toUserName+"]]>"+
                            "</FromUserName><CreateTime>"+new Date().getTime()+"</CreateTime><MsgType><![CDATA[text]]></MsgType>" +
                            "<Content><![CDATA[你好,你发的消息是："+content+"]]></Content></xml>";
                    logger.info("================weixin ret message:{}",recMessage);
                    return recMessage;
                }
                return "";
            }
        } catch (Exception e) {
            logger.error("weixinCheck error", e);
        }
        return "";
    }

}
