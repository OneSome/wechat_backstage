package com.yf.controller;

import com.yf.util.WeixinShareUtil;
import com.yf.vo.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <Description> 微信网页sdk演示，朋友和朋友圈分享<br>
 *
 * @author yinfeng<br>
 * @version 1.0<br>
 * @CreateDate 2018/8/1 <br>
 */
@Controller
@RequestMapping("/weChart")
public class WeixinSDKController {

    @Autowired
    private com.yf.service.TicketServiceImpl TicketServiceImpl;


    /**
     * logger <br>
     */
    private static Logger logger = LoggerFactory.getLogger(WeixinSDKController.class);

    /**
     * Description: 分享的页面<br>
     *
     * @author yinfeng <br>
     * @date   2018/8/2 <br>
     * @param  request request
     * @param  model model
     * @return java.lang.String <br>
     */
    @RequestMapping("/index")
    public String index(HttpServletRequest request, Model model) {

        Person single = new Person("hyj", 21);
        List<Person> people = new ArrayList<>();
        Person p1 = new Person("dlp", 21);
        Person p2 = new Person("tq", 21);
        Person p3 = new Person("mk", 21);
        people.add(p1);
        people.add(p2);
        people.add(p3);
        model.addAttribute("singlePerson", single);
        model.addAttribute("people", people);
        model.addAllAttributes(getWeiXinSignMap(request, null));
        return "greeting";
    }


    /**
     * 获取微信分享签名数据
     * 创建者  yinfeng
     * 创建时间    2017年7月14日 下午2:55:25
     *
     * @param request
     * @return
     * @throws UnsupportedEncodingException
     */
    protected Map<String, String> getWeiXinSignMap(HttpServletRequest request, String shareUrl) {

        String url = getCurrentUrlForWeiXin(request);
        if (!StringUtils.hasText(shareUrl))
            shareUrl = url;
        logger.info("shareUrl={},url={}", shareUrl, url);
        String jsapiCode = TicketServiceImpl.getTicket("ticket");
        logger.info("jsapiCode={}", jsapiCode);
        Map<String, String> weiXinSign = WeixinShareUtil.getWeiXinSign(url, jsapiCode);
        weiXinSign.put("shareUrl", shareUrl);
        weiXinSign.put("shareTitle", "我的分享");
        weiXinSign.put("shareDesc", "这是我写的一个分享小例子");
        weiXinSign.put("shareIcon", "http://wap.hb.10086.cn/wechat/images/other/charge_share_logo.png");
        logger.info("weiXinSign={}", weiXinSign);
        return weiXinSign;
    }


    /**
     * 获取当前url
     * 创建者  yinfeng
     * 创建时间    2017年7月14日 下午5:25:31
     *
     * @param request
     * @return
     */
    protected String getCurrentUrlForWeiXin(HttpServletRequest request) {
        String scheme = request.getScheme();
        String servletPath = request.getServletPath();
        String url = scheme + "://" + WeixinShareUtil.getUrlDomain() + servletPath;
        if (StringUtils.hasText(request.getQueryString())) {
            url = url + "?" + request.getQueryString();
        }
        return url;
    }
}
