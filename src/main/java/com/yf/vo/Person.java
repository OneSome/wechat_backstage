package com.yf.vo;

import lombok.Data;

/**
 * <Description> <br>
 *
 * @author yinfeng<br>
 * @version 1.0<br>
 * @CreateDate 2018/8/1 <br>
 */
@Data
public class Person {

    private String name;

    private Integer age;

    public Person() {
        super();
    }

    public Person(String name, Integer gae) {
        super();
        this.name = name;
        this.age = gae;
    }

}
