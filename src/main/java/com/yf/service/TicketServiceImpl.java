package com.yf.service;

import com.yf.util.WeixinShareUtil;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

/**
 * <Description> <br>
 *
 * @author yinfeng<br>
 * @version 1.0<br>
 * @CreateDate 2018/8/1 <br>
 */
@Component
public class TicketServiceImpl {


    private static final String DEMO_CACHE_NAME = "users";

    //查找票据
    @Cacheable(value = DEMO_CACHE_NAME, key = "#p0")
    public String getTicket(String ticket) {
        System.out.println("================================");
        String jsApiTicket = WeixinShareUtil.getJSApiTicket();
        return jsApiTicket;
    }
}
