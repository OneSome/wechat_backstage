package com.yf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {
		"com.yf"
})
@EnableCaching
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}



	/**
	 * Description: 优雅关闭服务<br>
	 *
	 * @author QSS<br>
	 * @taskId <br>
	 * @param confApp <br>
	 */
	private static void close(ConfigurableApplicationContext confApp) {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				if (confApp != null) {
					confApp.close();
				}
			}
		});
	}
}
