package com.yf.util;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

public class WeixinShareUtil {
    private static final Logger log = Logger.getLogger(WeixinShareUtil.class);
    private static String appId = "wx0da6831d5841294d";
    private static String appSecret = "50b7ec115d49eaaa254b1ba97be2d23d";
    private static String urlDomain = "yf123.applinzi.com/yf";

    public static Map<String, String> getWeiXinSign(String url, String jsapi_ticket) {
        Map<String, String> sign = Sign.sign(jsapi_ticket, url);
        sign.put("appId", appId);
        return sign;
    }

    public static String getJSApiTicket() {
        String jsApiTicket = JsapiTicketUtil.getJSApiTicket(appId, appSecret);
        return StringUtils.hasText(jsApiTicket) ? jsApiTicket : "";
    }

    public static String getUrlDomain() {
        return urlDomain;
    }

    public static void main(String[] args) {
        String jsApiTicket = WeixinShareUtil.getJSApiTicket();
        System.out.println(jsApiTicket);
    }


}
