package com.yf.util;

import com.alibaba.fastjson.JSONObject;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;


/**
 * 微信获取JsapiTicket
 * 创建者  yinfeng
 * 创建时间    2017年7月14日 下午2:10:46
 *
 * @version 1.0.0
 */
public class JsapiTicketUtil {

    private static final Logger log = Logger.getLogger(JsapiTicketUtil.class);

    /***
     * 模拟get请求
     *
     * @param url
     * @param charset
     * @param timeout
     * @return
     */
    public static String sendGet(String url, String charset, int timeout) {
        String result = "";
        try {
            URL u = new URL(url);
            try {
                URLConnection conn = u.openConnection();
                conn.connect();
                conn.setConnectTimeout(timeout);
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        conn.getInputStream(), charset));
                String line = "";
                while ((line = in.readLine()) != null) {

                    result = result + line;
                }
                in.close();
            } catch (IOException e) {
                log.error("JsapiTicketUtil.sendGet error url=" + url + " charset=" + charset + " timeout=" + timeout, e);
                return result;
            }
        } catch (MalformedURLException e) {
            log.error("JsapiTicketUtil.sendGet error url=" + url + " charset=" + charset + " timeout=" + timeout, e);
            return result;
        }

        return result;
    }

    /***
     * 模拟get请求
     *
     * @param url
     * @return
     */
    public static String sendPost(String url, String param) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            conn.setConnectTimeout(10000);
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(param);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送 POST 请求出现异常！" + e);
            e.printStackTrace();
        }
        //使用finally块来关闭输出流、输入流
        finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;

    }

    /***
     * 获取acess_token 来源www.vxzsk.com
     *
     * @return
     */
    public static String getAccessToken(String appid, String appSecret) {
//		appid = "wxd8a2d05dd0a27478";// 应用ID
//		appSecret = "222ef44cab212954afb235d504d41531";// (应用密钥)
        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="
                + appid + "&secret=" + appSecret + "";
        String backData = JsapiTicketUtil.sendGet(url, "utf-8", 10000);
        String accessToken = "";
        try {
            accessToken = JSONObject.parseObject(backData).getString(
                    "access_token");
        } catch (Exception e) {
            log.error("getAccessToken JSONObject error accessToken=" + accessToken, e);
        }
        return accessToken;
    }

    /***
     * 获取jsapiTicket 来源 www.vxzsk.com
     *
     * @return
     */
    public static String getJSApiTicket(String appid, String appSecret) {
        // 获取token
        String acess_token = JsapiTicketUtil.getAccessToken(appid, appSecret);
        String urlStr = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token="
                + acess_token + "&type=jsapi";
        String backData = JsapiTicketUtil.sendGet(urlStr, "utf-8", 10000);
        String ticket = "";
        try {
            ticket = JSONObject.parseObject(backData).getString("ticket");
        } catch (Exception e) {
            log.error("getJSApiTicket JSONObject error ticket=" + ticket, e);
        }
        return ticket;

    }

    public static void main(String[] args) {
        String accessToken11 = "12_FIRpx0j3yi3lYb9Dz1VQTMRLIQKfpCSbLd8iOHGVP3swjEZojbN8V936NLLfEEJTrZh_5vV_vudBfuFhVHBigZ10FNonKo8RWeoARst-nAE2NpC_GWb0mXIGLjTaVtSAk0sYa2m-7r6zuGvJRAQfABAGPF";
//        String accessToken = JsapiTicketUtil.getAccessToken("wx0da6831d5841294d", "50b7ec115d49eaaa254b1ba97be2d23d");
//        System.out.println(accessToken);
//		String jsapiTicket = JsapiTicketUtil.getJSApiTicket(appid, appSecret);
//		System.out.println("调用微信jsapi的凭证票为：" + jsapiTicket);
    /*	String a ="aa";
        Pattern p=Pattern.compile("[^,]*");
		Matcher m=p.matcher("null,a4afda471ef41bd6fb18f8003f52afc8,weixin");
		while(m.find()) {
		    System.out.println(m.group());
		}*/
        String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + accessToken11;
        String respData = JsapiTicketUtil.sendPost(url, "{" +
                "           \"touser\":\"odbj-0kUy1njaT8wdZJshqDfjLj4\"," +
                "           \"template_id\":\"GQyaA-vCzm5yx5jfa2H20YroW5c-hsN52aL0J1weY04\"," +
                "           \"url\":\"http://www.baidu.com\",  " +
                "           \"data\":{" +
                "                   \"name\": {" +
                "                       \"value\":\"yinfeng！\"," +
                "                       \"color\":\"#173177\"" +
                "                   }," +
                "                   \"age\":{" +
                "                       \"value\":\"26\"," +
                "                       \"color\":\"#173177\"" +
                "                   }" +
                "           }" +
                "       }");
        System.out.println(respData);

    }

}