package com.yf.util;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <Description> <br>
 *
 * @author yinfeng<br>
 * @version 1.0<br>
 * @CreateDate 2018/7/31 <br>
 */
public class MessageUtil {

    public static Map<String, String> parseXml(String xml) throws Exception {

        // 将字符串转为XML
        Document document = DocumentHelper.parseText(xml);
        // 将解析结果存储在HashMap中
        Map<String, String> map = new HashMap<>();
        // 得到xml根元素
        Element root = document.getRootElement();
        // 得到根元素的所有子节点
        List<Element> elementList = root.elements();
        // 遍历所有子节点
        for (Element e : elementList)
            map.put(e.getName(), e.getText());
        return map;
    }

}
