//随机串
var nonce_str = $("#nonce_str").val();
//时间戳
var timestamp = $("#timestamp").val();
//签名
var signature = $("#signature").val();
//公众号唯一标识
var appid = $("#appid").val();

wx.config({
	debug : true,//调试模式
	appId : appid,
	timestamp : timestamp,
	nonceStr : nonce_str,
	signature : signature,
	jsApiList: ['checkJsApi',
	            'openLocation',
	            'getLocation',
	            'onMenuShareTimeline',
	            'onMenuShareAppMessage']
});
wx.ready(function(){
    // config信息验证后会执行ready方法，所有接口调用都必须在config接口获得结果之后，config是一个客户端的异步操作，所以如果需要在页面加载时就调用相关接口，则须把相关接口放在ready函数中调用来确保正确执行。对于用户触发时才调用的接口，则可以直接调用，不需要放在ready函数中。
	console.log("ready");

})

wx.error(function(res){
    // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
    console.log("error:"+JSON.stringify(res));
});
