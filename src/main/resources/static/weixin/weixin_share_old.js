var lineLink ="";
var imgUrl = "http://wap.hb.10086.cn/wechat/images/other/charge_share_logo.png"; // 注意必须是绝对路径
//var lineLink = "http://wap.hb.10086.cn/wechat/weixinPayInitiator?type=init"; // 同样，必须是绝对路径
var descContent = '小伙伴，快来参与分享吧！'; // 分享给朋友或朋友圈时的文字简介
var sharetitle = "这是一个分享";// 分享给好友的title；

//当微信内置浏览器完成内部初始化后会触发WeixinJSBridgeReady事件。
document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
	
	var url = $("#imgUrl").val();
	var shareLink = $("#lineLink").val();
	var desc = $("#descContent").val();
	var title = $("#sharetitle").val();


	
	if(url!=null&&url!=''){
		imgUrl=url;
	}
	if(shareLink!=null&&shareLink!=''){
		lineLink=shareLink;
	}
	if(desc!=null&&desc!=''){
		descContent=desc;
	}
	
	if(title!=null&&title!=''){
		sharetitle=title;
	}
	
	// 发送给好友;
	WeixinJSBridge.on('menu:share:appmessage', function(argv) {
		WeixinJSBridge.invoke('sendAppMessage', {
			"img_url" : imgUrl,
			"img_width" : "640",
			"img_height" : "640",
			"link" : lineLink,
			"desc" : descContent,
			"title" : sharetitle
		}, function(res) {
			alert(JSON.stringify(res));
			var errMsg=res['err_msg'];
			if(errMsg==undefined||errMsg==null){
				errMsg=res['errMsg'];
			}
			var status=errMsg.split(":")[1];
			if(status=='ok' || status=='confirm'){
				alert("分享朋友成功"+JSON.stringify(res));
			/*	$.ajax({
					type : 'GET',
					url : saveLogUrl+"&shareType=1",
					dataType : 'json',
					cache : false,
					error : function() {
					},
					success : function(json) {
						if("0"==json.code && 1 == json.data){
							var shareSuccessWord=$("#shareSuccessWord").val();
				        	showMessage("提示",shareSuccessWord, 200, true, function(){
				        		location.reload();
				        	});
//				        	lottoeryObj.initPage(activityId);
				    	}
					}
				});*/
			}else if(status=='cancel'){
                alert("取消分享"+JSON.stringify(res));
			}else{
                alert("取消错误"+JSON.stringify(res));
			}
		});
	});

	// 分享到朋友圈;
	WeixinJSBridge.on('menu:share:timeline', function(argv) {
		WeixinJSBridge.invoke('shareTimeline', {
			"img_url" : imgUrl,
			"img_width" : "640",
			"img_height" : "640",
			"link" : lineLink,
			"desc" : descContent,
			"title" : sharetitle
		}, function(res) {
			alert(JSON.stringify(res));
			var errMsg=res['err_msg'];
			if(errMsg==undefined||errMsg==null){
				errMsg=res['errMsg'];
			}
			var status=errMsg.split(":")[1];
			if(status=='ok' || status=='confirm'){
				alert("分享朋友圈成功"+JSON.stringify(res));
				//分享成功之后的回调处理逻辑
			/*	$.ajax({
					type : 'GET',
					url : saveLogUrl+"&shareType=0",
					dataType : 'json',
					cache : false,
					error : function() {
					},
					success : function(json) {
						if("0"==json.code && 1 == json.data){
							var shareSuccessWord=$("#shareSuccessWord").val();
				        	showMessage("提示", shareSuccessWord, 200, true, function(){
				        		location.reload();
				        	});
//				        	lottoeryObj.initPage(activityId);
				    	}
					}
				});*/
			}else if(status=='cancel'){
                alert("取消分享"+JSON.stringify(res));
			}else{
                alert("取消错误"+JSON.stringify(res));
			}
		});
	});
}, false);